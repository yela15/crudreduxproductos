/* es el encargado de state de todo el proyecto 
    createStore : crea el store
    applyMiddleware: agrega a thunk como parte del store
    compose: 

    siempre que hay un store existe un reducer
*/
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducers'; // no se require colocar index >> es de forma automatica que lo reconoce

const store = createStore( // store se require en el componete principal >> para que fluya los datos
    reducer,
    compose(applyMiddleware(thunk), // vamos a usar thuk >> por eso applyMiddleware
        //https://github.com/zalmoxisus/redux-devtools-extension
        typeof window === 'object' &&
            typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== 'undefined' ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f // esto detectara si tenemos redux developer tools >> tambien funcionara si no lotienes instalado agregando el codigo
    )
);

export default store;