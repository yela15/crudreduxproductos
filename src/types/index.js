//  solamente describen que pasa en la aplicaion
// se usan en el action y el reducer

export const AGREGAR_PRODUCTO = 'AGREGAR_PRODUCTO';
export const AGREGAR_PRODUCTO_EXITO = 'AGREGAR_PRODUCTO_EXITO';
export const AGREGAR_PRODUCTO_ERROR = 'AGREGAR_PRODUCTO_ERROR';

/* ===================== agregando productos ===================== */
export const COMENZAR_DESCARGA_PRODUCTOS = 'COMENZAR_DESCARGA_PRODUCTOS';
export const DESCARGA_PRODUCTOS_EXITO = 'DESCARGA_PRODUCTOS_EXITO';
export const DESCARGA_PRODUCTOS_ERROR = 'DESCARGA_PRODUCTOS_ERROR';

/* ===================== Producto Eliminar ===================== */
export const OBTENER_PRODUCTO_ELIMINAR = 'OBTENER_PRODUCTO_ELIMINAR';
export const PRODUCTO_ELIMINADO_EXITO = 'PRODUCTO_ELIMINADO_EXITO';
export const PRODUCTO_ELIMINADO_ERROR = 'PRODUCTO_ELIMINADO_ERROR';

/* ===================== Producto Editar ===================== */
export const OBTENER_PRODUCTO_EDTAR = 'OBTENER_PRODUCTO_EDTAR';
export const COMENZAR_EDICION_PRODUCTO = 'COMENZAR_EDICION_PRODUCTO';
export const PRODUCTO_EDITADO_EXITO = 'PRODUCTO_EDITADO_EXITO';
export const PRODUCTO_EDITADO_ERROR = 'PRODUCTO_EDITADO_ERROR';

/* ===================== Producto Editar ===================== */
export const MOSTRAR_ALERTA = 'MOSTRAR_ALERTA';
export const OCULTAR_ALERTA = 'OCULTAR_ALERTA';
